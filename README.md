### circleGraph() plugin

**Description:** generate circular progress graphs with one line of HTML.  
**Requirements:** basic HTML/CSS/JS knowledge  
**Author:** HT (@&#x200A;glenthemes)

#### Preview:
![2 graphs next to each other, one says 69% and labelled "stress level" in uppercase. The other graph says 38% and is labelled "sanity level", also in uppercase.](https://file.garden/ZRt8jW_MomIqlrfn/screenshots/circle-graphs.png)

#### Demo:
:crystal_ball:&ensp;[circle-graph.gitlab.io/s/demo](https://circle-graph.gitlab.io/s/demo)

#### How to use:
1. Pick which div/selector you want to implement the graphs on.
- Make sure there's nothing in it!
- Add `fill-percentage="69%"` to it with the percentage you want, e.g.:
```html
<div class="myDiv1" fill-percentage="69%"></div>
<div class="myDiv2" fill-percentage="42%"></div>
```

2. Add the following either after `<head>`, before `</head>`, or before `</body>`:
```html
<!--✻✻✻✻✻✻  circular progress graphs by @glenthemes  ✻✻✻✻✻✻-->
<script src="https://circle-graph.gitlab.io/s/init.js"></script>
<link href="https://circle-graph.gitlab.io/s/core.css" rel="stylesheet">

<script>
circleGraph({
	selectors: ".myDiv1, .myDiv2", // div/selector you want to add the graph to, separated by commas
	showPercentage: true // can be true or false (no quotes "")
})
</script>

<style>
[fill-percentage]{
	--Circular-Graph-Size: 69px;
	--Circular-Graph-Fill: #222;
	--Circular-Graph-Empty: #eee;
	--Circular-Graph-Cover: #fff;
	--Circular-Graph-Line-Thickness: 3px;
}
</style>
```

4. Editing sizes and colors:
```css
[fill-percentage]{
	/* adjust options here */
}
```

If you want to style the percentage display (CSS):
```css
.circular_graph_percentage {
	/* add your own styling here */
}
```

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)
