/*-------------------------------------------------
    
    Circular progress graphs in pure JS + CSS
    - written by HT (@glenthemes)
	
    gitlab.com/circular-graph/s
    
-------------------------------------------------*/

window.circleGraph = function(params){
    let circleGraphInit = (params) => {
        let textSels = params.selectors.replaceAll(", ",",").split(",");
        let showPercentage = params.showPercentage;
        
        textSels.forEach(zel => {
            if(document.querySelector(zel) !== null){
                document.querySelectorAll(zel).forEach(sel => {
                    let fillPercentage;
                    if(sel.matches("[fill-percentage]") && sel.getAttribute("fill-percentage").trim() !== ""){
                        fillPercentage = Number(sel.getAttribute("fill-percentage").replace(/[^\d\.]*/g,""));

                        let cons = [".circular_graph_inner", ".cir_bac", ".cir_front"];

                        const noneYet = cons.every(div => !sel.querySelector(div));

                        if(noneYet){

                            let eCont = document.createElement("div");
                            eCont.classList.add("circular_graph_inner");
                            sel.appendChild(eCont);

                            let cir_back = document.createElement("div");
                            cir_back.classList.add("cir_back");
                            eCont.appendChild(cir_back)

                            let cir_front = document.createElement("div");
                            cir_front.classList.add("cir_front");
                            eCont.appendChild(cir_front)

                            if(showPercentage){
                                let cir_txt = document.createElement("div");
                                cir_txt.classList.add("circular_graph_percentage");

                                cir_front.appendChild(cir_txt)

                                let sqxkk = document.createElement("div");
                                sqxkk.classList.add("sqxkk")

                                cir_txt.appendChild(sqxkk)


                                let cp_num = document.createElement("span");
                                cp_num.classList.add("percentage_number")
                                cp_num.textContent = fillPercentage;
                                sqxkk.appendChild(cp_num)

                                let cp_mark = document.createElement("span");
                                cp_mark.classList.add("percentage_mark");
                                cp_mark.textContent = "%";
                                sqxkk.appendChild(cp_mark)

                            }

                            let percent_U50 = Math.floor((360 * (fillPercentage * 0.01)) + 90);
                            let percent_O50 = Math.floor((360 * (fillPercentage * 0.01)) - 90);

                            if(fillPercentage >= 0 && fillPercentage <= 50){
                                cir_back.style.backgroundImage = `linear-gradient(${percent_U50}deg, transparent 50%, var(--Circular-Graph-Empty) 50%), linear-gradient(90deg, var(--Circular-Graph-Empty) 50%, transparent 50%)`;
                            }

                            else if(fillPercentage > 50 && fillPercentage < 100){
                                cir_back.style.backgroundImage = `linear-gradient(${percent_O50}deg, transparent 50%, var(--Circular-Graph-Fill) 50%), linear-gradient(90deg, var(--Circular-Graph-Empty) 50%, transparent 50%)`;
                            }

                            else {
                                cir_back.style.backgroundImage = "none"
                            }

                        }//end: only do stuff if divs don't exist
                    }//end: if sel has [fill-percentage] attr
                })//end sel forEach
            }//end if sel exists
        })//end text sel forEach
    }//end circleGraphInit

	document.readyState == "loading" ?
    document.addEventListener("DOMContentLoaded", () => circleGraphInit(params)) :
    circleGraphInit(params);
}//end main func
